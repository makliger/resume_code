
import os, sys, django
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from ooc_mobile_api.models import MobileNotification, VacationTable, News, Tags, NewsAlert
from django.contrib.auth.models import User, Group
import pytz
from django.utils import timezone
from nltk.tokenize import word_tokenize
import nltk
nltk.download('punkt')


users = User.objects.all()
tags = Tags.objects.filter(is_active=True)
userTags = {}
userArticles = {}
distinctTags = []
for user in users:
    userArticles[user.pk] = []
    utags = tags.filter(user_id=user)
    temp = []

    for ut in utags:
        temp.append(ut.tag_name)
        distinctTags.append(ut.tag_name)

    userTags[user.pk] = temp
### get tags 

distinctTags = set(distinctTags)
today = timezone.now().date()
seenAlready = NewsAlert.objects.using('logging').all()
seen = []
for article in seenAlready:
    seen.append(article.news_id)

### get new articles
articles = News.objects.exclude(pk__in=seen)
results = {}

#### get stop words
stoppath = os.path.join(sys.path[0], 'stop_words.txt')
fd = open(stoppath, 'r')
content = fd.read()
stop_words = set(word_tokenize(content))


## get tags that appear in each article
for article in articles:
    words = set(word_tokenize(article.contents))
    words = words - stop_words
    intersection = distinctTags & words ### smaller set on left (performance boost if len(LeftSet) < 4
    if intersection:
        results[article.pk] = intersection
    alert = NewsAlert(news_id=article.pk) ## weve seen this article so add it to the seen table
    alert.save(using='logging')


for article, keywords in results.items(): ### on each article, loop through users tags
    for id, tags in userTags.items():
        intersect =  set(tags) & keywords
        if intersect:
            userArticles[id].append(article)
           

log = []
for uid, articleList in userArticles.items():
    if articleList:
        user = User.objects.get(pk=uid)
        nt = MobileNotification(target_user_id=user, target_publish_start=timezone.now(), msg_header="There are new articles today with keywords you follow! ", msg_body=' ', is_published=False)
        nt.save()
        log.append(nt.target_user_id)


print("news_alert")
print(str(timezone.now()))
print(str(log))

