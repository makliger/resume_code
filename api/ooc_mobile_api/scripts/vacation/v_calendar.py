import os, sys, django
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from ooc_mobile_api.models import MobileNotification, VacationTable, VacationDateSchedule
from django.contrib.auth.models import User, Group
import pytz
from django.utils import timezone
from datetime import date

users = User.objects.all()
vacation = VacationDateSchedule.objects.using('logging').filter(is_active=True)
todays_date = timezone.now().date()

###########
results = ["removed from vacation", str(todays_date)]

for user in vacation:
    return_date = date(user.year, user.month, user.day)
    delta = return_date - todays_date
    if delta.days <= 0:
        user.is_active= False
        vt_user = VacationTable.objects.get(user_id_id=user.user_id)
        vt_user.status = False
        vt_user.save()
        user.save(using='logging')
        results.append(str(user))
    print(delta)
print(str(results))

