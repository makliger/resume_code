from ooc_mobile_api.models import News, Tags, NewsTags, NewsFavorite, Breakfast, MobileNotification
from django.utils import timezone
from django.contrib.auth.models import User, Group
import os, pytz
from faker import Faker
fake = Faker()
fake.seed(12345)

#### returns map of lists of fake data  of length count
def F_User(count):
    res = {'username':[], 'password':[]}
    for i in range(0, count-1):
        res['username'].append(fake.word())
        res['password'].append(fake.word())
    return res

def F_Group(count):
    res = {'name':[]}
    for i in range(0, count-1):
        res['name'].append(fake.word())
    return res


def F_News(count):
    res = {'news_date': [], 'contents' : [], 'last_modified' : []}
    for i in range (0, count-1):
        res['news_date'].append(fake.date_this_month())
        res['contents'].append(fake.sentence())
        res['last_modified'].append(fake.date_time_between(start__date="-1d", end_date="now"))
    return res

#### news_date, contents, last_modified

def F_Tags(count):
####tag_name
    res = {'tag_name': [] }
    for i in range (0, count-1):
        res['tag_name'].append(fake.word())
    return res


def F_NewsTags(count):
### news, tag, last_modified
    res = {'news': F_News(count), 'tag' : F_Tags(count) , 'last_modified' : []}
    for i in range (0, count-1):
        res['last_modified'].append(fake.date_time_between(start__date="-1d", end_date="now"))
    return res


def F_NewsFavorite(count):
#### news_id, user_id, is_active, last_modified
    res = {'news_id': F_News(count), 'is_active': [], 'user_id': F_User(count), 'last_modified' : []}
    for i in range (0, count-1):
        res['is_active'].append(i % 3 == 1)
        res['last_modified'].append(fake.date_time_between(start__date="-4w", end_date="now"))
    return res

def F_Breakfast(count):
#### user_id, order, ordered_datetime
    res = {'user_id': F_User(count), 'order' : [], 'ordered_datetime' : []}
    for i in range (0, count-1):
        res['order'].append(fake.sentence())
        res['ordered_datetime'].append(fake.date_time_between(start__date="-2w", end_date="now"))
    return res

def MobileNotification(count):
#### target_user_id, target_user_group_id, target_publish_start, msg_body, is_published
    res = {'target_user_id': F_User(count),'target_user_group_id' : F_Group(count), 'target_publish_start' : [], 'msg_header': [], 'is_published':[]}
    for i in range (0, count-1):
        res['msg_header'].append(fake.sentence())
        res['target_publish_start'].append(fake.date_time_between(start__date="-1d", end_date="+1d"))
        res['is_published'].append(i % 2 == 0)
    return res


def F_Logs(count):
    res = {'log': [], 'level' : []}
    for i in range(0, count-1):
        res['log'].append(fake.sentence())
        res['level'].append((timezone.now().hour + i) % 3)
    return res
