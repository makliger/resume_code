import factory
import os, pytz
from ooc_mobile_api.models import MobileNotification, News, NewsFavorite, Tags, NewsTags, VacationTable, LogTable, LogInfoTable, Breakfast, TopsPmTargets, TopsPmTargetGroups, TopsPmEventStats, TopsPmTargetSummaryStats, TopsPmProcessStats
from django.utils import timezone
from django.contrib.auth.models import User, Group
from fcm_django.models import FCMDevice
from django.test import Client
from datetime import timedelta
from ooc_mobile_api import services, notifications, logs, create_data




class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User
    

class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group

class MobileNotificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MobileNotification
    target_user_id= factory.SubFactory(UserFactory)
    target_user_group_id = factory.SubFactory(GroupFactory)
class NewsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = News

class TagsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Tags

class NewsFavoriteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = NewsFavorite
    user_id=factory.SubFactory(UserFactory)
    news_id = factory.SubFactory(NewsFactory)
class NewsTagsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = NewsTags
    news=factory.SubFactory(UserFactory)
    tag = factory.SubFactory(TagsFactory)
class BreakfastFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Breakfast
    user_id = factory.SubFactory(UserFactory)

class FCMDeviceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FCMDevice
    user=factory.SubFactory(UserFactory)
