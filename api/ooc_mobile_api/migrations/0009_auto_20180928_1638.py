# Generated by Django 2.1.1 on 2018-09-28 16:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ooc_mobile_api', '0008_auto_20180920_2110'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='topspmtargetsummarystats',
            name='id',
        ),
        migrations.AddField(
            model_name='topspmtargetsummarystats',
            name='is_restarting',
            field=models.NullBooleanField(db_column='is_restarting'),
        ),
        migrations.AddField(
            model_name='topspmtargetsummarystats',
            name='local_pid',
            field=models.BigIntegerField(blank=True, db_column='local_pid', null=True),
        ),
        migrations.AddField(
            model_name='topspmtargetsummarystats',
            name='target',
            field=models.ForeignKey(default=-1, on_delete=django.db.models.deletion.SET_DEFAULT, to='ooc_mobile_api.TopsPmTargets'),
        ),
        migrations.AlterField(
            model_name='breakfast',
            name='no_breakfast',
            field=models.NullBooleanField(db_column='no_breakfast'),
        ),
        migrations.AlterField(
            model_name='topspmtargetsummarystats',
            name='has_exited',
            field=models.NullBooleanField(),
        ),
        migrations.AlterField(
            model_name='topspmtargetsummarystats',
            name='is_responding',
            field=models.NullBooleanField(),
        ),
        migrations.AlterField(
            model_name='topspmtargetsummarystats',
            name='summary_id',
            field=models.BigAutoField(primary_key=True, serialize=False),
        ),
    ]
