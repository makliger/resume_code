# Generated by Django 2.1.1 on 2019-09-19 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ooc_mobile_api', '0015_auto_20190919_1117'),
    ]

    operations = [
        migrations.CreateModel(
            name='MobileBreakfast',
            fields=[
                ('order_id', models.AutoField(primary_key=True, serialize=False)),
                ('order_details', models.CharField(blank=True, max_length=512, null=True)),
                ('order_datetime', models.DateTimeField()),
                ('order_date', models.DateField(blank=True, null=True)),
            ],
            options={
                'db_table': 'mobile_breakfast',
                'managed': False,
            },
        ),
    ]
