
import os, sys, django
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from ooc_mobile_api.models import MobileNotification, VacationTable, Breakfast
from django.contrib.auth.models import User, Group
import pytz
from django.utils import timezone


users = []
vt = VacationTable.objects.filter(status=False)
for person in vt:
    order = Breakfast.objects.filter(user_id=person.user_id).filter(order_date=timezone.now().date()).last()
    if not order:
        users.append(person.user_id)

for user in users:
    nt = MobileNotification(target_user_id=user, target_publish_start=timezone.now(), msg_body="Your last order will be used otherwise.", msg_header='Please submit your order!', is_published=False)
    nt.save()


print("init")
print(str(timezone.now()))
print(str(users))


