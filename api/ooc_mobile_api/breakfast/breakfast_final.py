import os, sys, django
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from ooc_mobile_api.models import MobileNotification, VacationTable, Breakfast
from django.contrib.auth.models import User, Group
import pytz
from django.utils import timezone
from django.db.models import Q

users = []
vt = VacationTable.objects.filter(status=False)
before_seven = Q(ordered_at__hour=0)
for i in range(1,7):
    before_seven= before_seven | Q(ordered_at__hour=i)



for person in vt:
    todayOrder = Breakfast.objects.filter(user_id=person.user_id).filter(order_date=timezone.now().date()).filter(before_seven).last()
    if not todayOrder:
        users.append(person.user_id)

for user in users:
    nt = MobileNotification(target_user_id=user, target_publish_start=timezone.now(), msg_body="Your last order will be used", msg_header='You still have not submitted your order!', is_published=False)
    nt.save()
print("final reminder")
print(str(timezone.now()))
print(str(users))



