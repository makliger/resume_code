### logs.py ->> input : filename, corresponding target_id
### reads from plain text with logs, parses, inserts to LogTable
### views.py will take latest from LogTable
### this script will be run frequently to keep LogTable up to date

### Alternatively, we can just read from tail of each log file
### and send response. Feasability test required. Return Json
## from this function to logs in view.py
## this way we can just read target_id from mobile
## will need to store filepaths in some config file or read it from db (better)

### fileserver will be persistently mounted
##### the next few lines are critical - they allow this script to be executed as a standalone process
##### we will call it from the crontab to insert logs into the database directly from the fileserver or call from views.py


import os, re, django, sys, pytz
import datetime
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from ooc_mobile_api.models import LogTable, LogInfoTable
from django.utils import timezone

## ==============================================================
## ==============================================================

def tail_logs(target, size):
    arguments = sys.argv ### Target id followed by filepath
    tid = target
    try:
        info = LogInfoTable.objects.using('logging').get(target=tid)
    except LogInfoTable.DoesNotExist:
        return 1
    if not info:
        return 1
   # readsize = info.size
    path = info.path
    file_p = os.path.join('/home/oldorchard/shared/', path)
    try:
        sz = os.path.getsize(file_p)
    except os.error:
        return "Does not exist or inaccessible"

    if(sz <= size):
        file_desc = open(file_p, "r")
    else:
        file_desc = open(file_p, "r")
        file_desc.seek(0, os.SEEK_END) ###seek to end, then move backwards
        file_desc.seek(file_desc.tell() - size, os.SEEK_SET) ### get file position and seek  pos - size (a positive number) 
        ###### read last size bytes OR read all bytes if file size is not big enough





#    file_path = os.path.join(settings.STATIC_ROOT, 'logs/mylogfile.log_1')
    data = file_desc.read()
    exp = re.compile("\d{2,4}\-\d{1,2}\-\d{1,2}\s*\d{1,2}\:\d{1,2}\:\d{1,2},\d{1,3}\s*\[\s*\d{1,3}\s*\|\s*[A-Za-z]{1,10}\s*\|\s*.*?\].*?(?=\d{2,4}\-\d{1,2}\-\d{1,2}\s*\d{1,2}\:\d{1,2}\:\d{1,2},\d{1,3}\s*\[\s*\d{1,3}\s*\|\s*[A-Za-z]{1,10}\s*\|\s*.*?\]|\Z)", re.DOTALL)
    logs = exp.findall(data)
    levels = [0] * len(logs)
    tobeJson = [] ## todo truncate strings of length n
    for index, log in enumerate(logs, start=0):
        if("| INFO |" not in log):
            if("| WARN |" not in log):
                levels[index] = 2
            else:
                levels[index] = 1
        tobeJson.append({'level': levels[index], 'body':log, 'time' : timezone.now().strftime("%m/%d/%Y, %H:%M:%S")}) ## note time is only here to differentiate logs
   # logz = (LogTable(body=tobeJson[i]['body'],level=tobeJson[i]['level'], time=tobeJson[i]['time']) for i in range(len(logs)))
   # logz = list(logz)
    return(tobeJson)
