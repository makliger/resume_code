import os, re, django, sys, pytz
import datetime
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings
from datetime import timedelta, datetime
from ooc_mobile_api.models import MobileNotification
from django.utils import timezone
from django.contrib.auth.models import User, Group
from fcm_django.models import FCMDevice
#### fcm admin modules for heads up notifications
import firebase_admin
from firebase_admin import messaging, credentials, exceptions

#### initialize firebase app
cred = credentials.Certificate('/webapps/ooc_server/ooc_mobile/keedz/serviceAccountKey.json')
app = firebase_admin.initialize_app(cred)
sys.stdout.write(app.name)


search_range = 10 ## this is in days
all_msgs = MobileNotification.objects.filter(is_published=False).filter(target_publish_start__range=[(timezone.now()-timedelta(days=search_range)), timezone.now()])
### get all unpublished messages within range at top of script
### could have passed local variable to two functions but did not
### want to worry if python would complain about queryset obj  being passed around


### this script will be run often enough during off hours, and probably not as often during office hours
def send_msg(title, body, token):
    message = messaging.Message(notification=messaging.Notification(title=title, body=body),android=messaging.AndroidConfig(priority="high", notification=messaging.AndroidNotification(channel_id="1", click_action='FLUTTER_NOTIFICATION_CLICK', ),data = {'title':title, 'body':body},),token=token)
    try:
        result = messaging.send(message)
    except exceptions.FirebaseError as err:
        result = 'fail'
    except ValueError:
        sys.stdout.write(str(message))
        result = "invalid"
    return result

def send_group_msg(title, body, tokens):
    messages = []
    for t in tokens:
        messages.append(messaging.Message(notification=messaging.Notification(title=title, body=body),android=messaging.AndroidConfig(priority="high", notification=messaging.AndroidNotification(channel_id="1", click_action='FLUTTER_NOTIFICATION_CLICK'),),token=t))
    try:
        result = messaging.send_all(messages)
    except exceptions.FirebaseError:
        result = "fail"
    except ValueError:
        result = "invalid"
    return result

def groups():
    msgs = (all_msgs.filter(target_user_id__isnull=True) | all_msgs.filter(target_user_id=-1)).distinct()
    groups = Group.objects.all()
    group_ids = []
    tokens = []
    devices = []
    results = []
## get devices in groups, then send messages
    for g in groups:
        group_ids.append(g.pk)
        users = User.objects.filter(groups__id=g.pk)
        group_device = FCMDevice.objects.filter(user__in = users)
        group_tokens = []
        for device in group_device:
            group_tokens.append(device.registration_id)
        tokens.append(group_tokens)

    for msg in msgs:
        ## set message parameters for clarity
        if msg.msg_body is None:
            m_body = ""
        else:
            m_body = msg.msg_body
        m_body = m_body + "\n" + msg.target_publish_start.strftime("%m/%d/%Y, %H:%M:%S")

        m_title = msg.msg_header
        if msg.msg_header is None:
            m_title = " "
        m_subtitle = msg.msg_footer
        if msg.msg_footer is None:
            m_subtitle = " "
        gid = msg.target_user_group_id_id ## loop through each message
        if gid == -1:
            gid =1
        i = 0
        for id in group_ids: ## now find the group we need to send to
            if id == gid:
                res = send_group_msg(title=m_title, body=(m_body+" " + m_subtitle), tokens=tokens[i])
                results.append(res)
        ### save new state of message (is published)
                if res is not "fail" and res is not "invalid":
                    msg.is_published = True
                    msg.save()
                else:
                    pass ##### LOG HERE
                break
            else:
                i= i + 1

    return results


def users():
    msgs = all_msgs.exclude(target_user_id__isnull=True).exclude(target_user_id=-1)
    devices = FCMDevice.objects.all()
    results = []
    for msg in msgs:
        m_body = msg.msg_body
        if msg.msg_body is None:
            m_body = " "
        else:
            m_body = m_body + "\n"  + msg.target_publish_start.strftime("%m/%d/%Y, %H:%M:%S")
        m_title = msg.msg_header
        if msg.msg_header is None:
            m_title = " "
        m_subtitle = msg.msg_footer
        if msg.msg_footer is None:
            m_subtitle = " "
        sys.stdout.write(str(msg.target_user_id))
        sys.stdout.flush()
        try:
            device = devices.get(user=msg.target_user_id)
        except FCMDevice.DoesNotExist:
             results.append("device not registered yet:" + str(msg.target_user_id_id))
             continue
        m_token = device.registration_id
        res = send_msg(title=m_title, body=m_body+" "+m_subtitle, token=m_token)
        results.append(str(res))
        if res is not "fail" and res is not "invalid":
            msg.is_published=True
            msg.save()
        else:
            pass ##### LOG HERE

    return results


def main():
    ## we need to check for group messages and user messages
    res_g = groups()
    res_u = users()
    time = []
    time.append(" --- ---  " + "sent at: " + timezone.now().strftime("%m/%d/%Y, %H:%M:%S"))
    if res_g or res_u:
        print(res_g + res_u +  time)

if __name__ == '__main__':
    main()


    ## notifications model interface for reference
    #notification_id = models.BigAutoField(primary_key=True)
    #msg_header = models.CharField(max_length=256, blank=True, null=True)
    #msg_body = models.CharField(max_length=512)
    #msg_footer = models.CharField(max_length=256, blank=True, null=True)
    #target_user_id = models.ForeignKey(settings.AUTH_USER_MODEL,db_column='target_user_id', default=-1, on_delete=models.SET_DEFAULT, blank=True, null=True)
    #target_user_group_id = models.ForeignKey(Group, db_column='target_user_group_id',  default=-1, on_delete=models.SET_DEFAULT, blank=True, null=True)
    #target_publish_start = models.DateTimeField()
    #is_published = models.BooleanField()
    #creation_datetime = models.DateTimeField(default= timezone.now())
