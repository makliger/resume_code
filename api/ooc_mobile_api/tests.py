from django.test import TestCase
import os, pytz
from ooc_mobile_api.models import MobileNotification, News, NewsFavorite, Tags, NewsTags, VacationTable, LogTable, LogInfoTable, Breakfast, TopsPmTargets, TopsPmTargetGroups, TopsPmEventStats, TopsPmTargetSummaryStats, TopsPmPrxocessStats
from django.utils import timezone
from django.contrib.auth.models import User, Group
from fcm_django.models import FCMDevice
from django.test import Client
from datetime import timedelta
from ooc_mobile_api import services, notifications, logs

################ Model Unit Tests 

## MobileNotification, VacationTable, LogInfoTable, Breakfast, News, NewsTags, Tags, NewsFavorite, (TBD TargetRestart)

#class NotificationTestCase(TestCase):
#    def setUp(self):
#        self.notification = MobileNotification.objects.create(msg_header="test_g_mg", target_user_group_id_id=1. is_published=False, target_publish_start=timezone.now())



############ View Integration Tests

class AuthenticationTestCase(TestCase):
    def setUp(self):
        User.objects.create_user(username='test', password='1234')

    def tryLogin(self):
        user = User.objects.get(username='test')



####### function unit tests that are used within view 

## get_latest_three_orders
class BreakfastOrdersTestCase(TestCase):
    def setUp(self):
        User.objects.create(username='test', password='test_pass')
        user = User.objects.last()
        time=timezone.now()-timedelta(days=1)
        time.hour = 2
        Breakfast.objects.create(user_id=user,ordered_at=time, order_date=time.date(), order="test")
        time=timezone.now()-timedelta(days=5)
        time.hour = 2
        Breakfast.objects.create(user_id=user,ordered_at=time, order_date=time.date(), order="test")
        time=timezone.now()-timedelta(days=2)
        time.hour = 10
        Breakfast.objects.create(user_id=user,ordered_at=time, order_date=time.date(), order="test")
        time=timezone.now()-timedelta(days=40)
        Breakfast.objects.create(user_id=user,ordered_at=time, order_date=time.date(), order="test")


    def test_correct_orders_displayed(self):
        last_order = Breakfast.objects.latest('ordered_at')
        shown_orders = services.get_three_latest_orders(User.objects.last(), 10)
        earliest_order = Breakfast.objects.earliest('ordered_at')
        self.assertEqual(shown_orders.count(),2)
        self.assertEqual(last_order.order_date, (timezone.now()-timedelta(days=1)).date())
        self.assertEqual(shown_orders.filter(id=earliest_order.id).exists(), False)


class PublishedAlertsTestCase(TestCase):
    def setUo(self):
        user = User.objects.create(username="test", password="test")
        group = Group.objects.create(name="testers")
        group.user_set.add(user)
        titles = ["test1", "test2", "test3", "test4"]
        users = [user, None, user, None]
        groups = [group, group, None, None]
        times = [timezone.now(), timezone.now()-timedelta(days=1), timezone.now()-timedelta(days=2), timezone.now()-timedelta(days=20)]
        msgs = []
        for i in range (0,3):
            msgs.append(MobileNotification.objects.create(msg_header=titles[i], is_published=True, target_publish_start=times[i], target_user_id=users[i], target_user_group_id=groups[i])




    def test_correct_messages_returned(self):
         results = services.get_all_messages(User.objects.get(username="test"), 15)
         self.assertEqual(results.count(), 3)
         too_old = MobileNotification.earliest('target_publish_start')
         self.assertEqual(results.filter(id=too_old.id).exists(), False)


class NewsFavoriteUpdateTestCase(TestCase):
    def setUp(self):
        news = News.objects.create(news_body="test", news_date = timezone.now().date()-timedelta(days=10), contents = "test", last_modified=timezone.now())
        users = [User.objects.create(username="testone", password="one"), User.objects.create(username="testtwo", password="two")]
        nts = [NewsFavorite.objects.create(user_id=users[0], news_id=news, is_active=True, last_modified=timezone.now()), NewsFavorite.objects.create(user_id=users[1], news_id=news, is_active=False, last_modified=timezone.now())]

    def test_tag_updates(self):
        result = services.update_news_favorite(News.objects.first(), User.objects.first())
        res = services.update_news_favorite(News.objects.first(), User.objects.last())
        self.assertEqual(res, True)
        self.assertEqual(result, True)
        self.assertEqual(NewsFavorite.objects.first().is_active, not NewsFavorite.objects.last().is_active)
        self.assertEqual(NewsFavorite.objects.get(user_id=User.objects.get(username="testone")).is_active, False)


def UserNewsTestCase(TestCase): ## test news returned to user is correct

   def setUp(self):
       user = User.objects.create(username="test", password="test")


   def test_user_news(self):



####### Fake data unit tests

