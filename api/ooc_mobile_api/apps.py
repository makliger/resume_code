from django.apps import AppConfig


class OocMobileApiConfig(AppConfig):
    name = 'ooc_mobile_api'
