from ooc_mobile_api.models import Tags, NewsTags,News, NewsFavorite, Breakfast, TopsPmTargets,  TopsPmTargetGroups, TopsPmEventStats, TopsPmTargetSummaryStats, TopsPmProcessStats, LogTable, VacationTable, MobileNotification
import json, datetime
from datetime import timedelta
from django.db import connections
import os, re
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.utils import timezone
import pytz
from django.db.models import Q
from fcm_django.models import FCMDevice


#### business logic file for complicated queries 



def get_three_latest_orders(user):
    ####### code to extract last 3 distinct (via order parameter) latest orders from each day at time t am in the last 30 days
    rangeStart=1
    rangeEnd=2
    orders = []
    bodies = []
    n = len(orders)
    before_nine = Q(ordered_at__hour=0)
    for hour in range(1, 10):
        before_nine = before_nine | Q(ordered_at__hour=hour)


    usr_orders = Breakfast.objects.filter(user_id=user)
    while( n < 3 and rangeEnd < 30):
        order = usr_orders.filter(order_date__range=[(timezone.now().date()-timedelta(days=rangeEnd)), (timezone.now().date()-timedelta(days=rangeStart))]).filter(before_nine)
        if not order:
            rangeEnd+=1
            rangeStart+=1
        elif(order.latest('ordered_at') in orders):
            rangeEnd+=1
            rangeStart+=1
        elif(order.latest('ordered_at').order in bodies):
            rangeEnd+=1
            rangeStart+=1
        else:
            orders.append(order.latest('ordered_at'))
            bodies.append(order.latest('ordered_at').order)
            n+=1
            rangeEnd+=1
            rangeStart+=1

    return orders



### gets all alerts published to a user within range (days, default=10)
def get_all_messages(user, range):
    if not range:
        range=10
    groups = []
        ## GET GROUP AND USER MSGS SEPARATE, THEN COMBINE DISTINCT
    msgs_us = MobileNotification.objects.filter(target_user_id=user)
    for g in user.groups.all():
        groups.append(g.pk)
    msgs_gr = MobileNotification.objects.filter(target_user_group_id__in=groups)
    msgs = (msgs_us | msgs_gr).distinct()
    msgs = msgs.filter(target_publish_start__range=[timezone.now()-timedelta(days=range), timezone.now()])
    msgs = msgs.order_by('-target_publish_start')
    msgs = msgs.filter(is_published=True) ## in case tghe publishing system fails, lets make it consistent
    return msgs



## updates or creates newstag affiliation given two valid parameters
def update_news_favorite(user, news):
    try:
        obj = NewsFavorite.objects.get(user_id=user, news_id=news)
        obj.is_active= not obj.is_active
        obj.last_modified= timezone.localtime()
        obj.save(update_fields=['is_active', 'last_modified'])
        return True
    except NewsFavorite.DoesNotExist:
        obj = NewsFavorite(news_id=news, user_id=user, is_active=True, last_modified=timezone.now())
        obj.save()
        return True

## get user news RETURNS SERIALIZED DATA MAP to accomadate bookmarks
def user_news(user, range):
    if not range:
        range = 10
    try:
        fav = NewsFavorite.objects.filter(user_id=us[0],is_active=True)
    except NewsFavorite.DoesNotExist:
        fav = ((),)
    f = News.objects.filter(news_id__in =fav)
    p = News.objects.filter(news_date__range=[timezone.now()-timedelta(days=range),timezone.now()])
    fin = (f | p).distinct()
    res = fin.order_by('-news_date')
    serializer = NewsSerializer(res, many = True)
    fserializer = NewsFavoriteSerializer(f, many = True)
    return {'all': serializer.data, 'favorites' : fserializer.data}

