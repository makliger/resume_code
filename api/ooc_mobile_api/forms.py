from django.forms import ModelForm
from ooc_mobile_api.models import MobileNotification
from django.contrib.auth.models import User

class NotificationForm(ModelForm):
    class Meta:
        model = MobileNotification
        fields = ['msg_header', 'msg_body', 'msg_footer', 'target_user_id', 'target_user_group_id', 'target_publish_start']

