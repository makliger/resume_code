from rest_framework import pagination


class LowPagination(pagination.PageNumberPagination):
    page_size =10

class ModeratePagination(pagination.PageNumberPagination):
    page_size=25

class HighPagination(pagination.PageNumberPagination):
    page_size=100
