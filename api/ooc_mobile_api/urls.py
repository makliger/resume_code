from ooc_mobile_api import views
from django.urls import path
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth import views as auth_views
from django.conf.urls import include
urlpatterns = [
    path('broadcast_template/', views.broadcast_template),
    path('deadline/', views.DeadlineListCreate.as_view(), name='deadline'),
    path('deadline/<int:pk>', views.DeadlineUpdate.as_view(), name='deadline_detail'),
    #path('deadline/', views.deadline),
    path('usergroups/', views.get_user_groups),
    path('broadcast/<int:pk>', views.broadcast),
    path('breakfast/', views.todays_orders),
    path('breakfast_mobile/', views.mobile_todays_orders),
    path('usertags/<int:pk>', views.UserTagsAdminView.as_view(), name="tags admin"),
    path('usertags/', views.UserTagsListCreate.as_view(), name = 'user tags'),
    path('usertags/change/<int:pk>', views.UserTagsUpdate.as_view(), name= 'user tags update'),
    path('remove_tag/<int:pk>', views.remove_tag),
    path('login/', views.login),
    path('test/', views.test_auth),
    path('process/', views.plist),
    path('admin/', admin.site.urls),
    path('news/<int:id>/', views.news),
    path('save_news/<int:pk>/', views.save_news),
    path('events/<int:id>/', views.events),
    path('orders/<int:pk>', views.orders),
    path('submit_order/<int:pk>/', views.submit_order),
    path('process/<int:id>/', views.pdetail),
    path('logs/<int:id>/', views.logs),
    path('process_restart/<int:pk>/', views.process_restart),
    path('vacation_mode/<int:pk>/', views.submit_vacation_status),
    path('messages/<int:pk>/', views.messages),
    path('notification/', views.NotificationList.as_view(), name='notification_add'),
    path('notification/<int:pk>/', views.NotificationDetail.as_view(), name='notification_detail'),
    path('register_device/<int:pk>', views.register_device),
    path('tags', views.TagsListView.as_view(), name='tags_list'),
    path('add_tags/<int:pk>', views.add_user_tag, name='tag_create_list'),
    path('news_tags', views.NewsTagsListView.as_view(), name='news_tags_list'),
    path('news_lookup', views.NewsSearchListView.as_view(), name = 'news_search'),
    path('news_by_tags', views.NewsSearchByTagsListView.as_view(), name = "news_search_by_tags"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('password-change/', auth_views.PasswordChangeView.as_view(),  name='password_change'),
    path('password-change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
#    path('password_reset/confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
#    path('password_reset/complete/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),


]

urlpatterns = format_suffix_patterns(urlpatterns, allowed = ['json'])
