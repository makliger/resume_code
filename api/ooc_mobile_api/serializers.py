from django.contrib.auth.models import User, Group
from rest_framework import serializers
from ooc_mobile_api.models import Tags, NewsTags,News, NewsFavorite, Breakfast, TopsPmTargets,  TopsPmTargetGroups, TopsPmEventStats, TopsPmTargetSummaryStats, TopsPmProcessStats, LogTable, MobileNotification, Deadline, BroadcastTemplate
class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model =  LogTable
        fields = '__all__'

class DeadlineSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Deadline
        fields = '__all__'

class BroadcastTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model =  BroadcastTemplate
        fields = ('title', 'message')

class TargetGraphsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopsPmProcessStats
        fields = ('handle_count', 'current_working_set', 'private_memory','total_processor_time') 
# TODO is restarting and graphs
class TargetTilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopsPmTargetSummaryStats
        fields = ('handle_count', 'last_modified', 'current_working_set', 'private_memory', 'local_pid', 'cpu_usage', 'is_responding', 'has_exited', 'server_memory', 'server_cpu_usage', 'is_restarting' )


class TargetSerializer(serializers.ModelSerializer):
    group_name  = serializers.CharField(source='target.target_category', read_only=True)
    target_name = serializers.CharField(source='target.target_name', read_only=True)
    class Meta:
        model = TopsPmTargetSummaryStats
        fields = ( 'status', 'target', 'group_name','machine_name', 'target_name', 'last_modified' )
        #'run_after', 'depends_on_ids', 'status', 'date_created', 'last_modified')
class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'

class NewsFavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('news_id',)
class ProcessGraphSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopsPmProcessStats
        fields = ('virtual_memory', 'private_memory', 'current_working_set', 'total_processor_time')
class ProcessSummaryStats(serializers.ModelSerializer):
    class Meta:
        model = TopsPmTargetSummaryStats
        fields = '__all__'
class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopsPmEventStats
        fields = ('stats_id', 'event_id', 'process_id', 'event_source', 'event_datetime')

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Breakfast
        fields = ('order',  'order_date')
class MobileNotificationCreateSerializer(serializers.ModelSerializer):
    class Meta:
         model = MobileNotification
         fields = ('msg_header', 'msg_body', 'msg_footer', 'target_user_id', 'target_user_group_id', 'target_publish_start')
class MobileNotificationListSerializer(serializers.ModelSerializer):
    class Meta:
         model = MobileNotification
         fields = '__all__'

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
         model = Group
         fields = ('pk', 'name',)


class MobileNotificationAppSerializer(serializers.ModelSerializer): ## used for mobile app only
    class Meta:
          model = MobileNotification
          fields = ('msg_header', 'msg_body', 'msg_footer', 'target_user_group_id', 'target_publish_start')

class TagsSerializer(serializers.ModelSerializer):
    class Meta:
         model = Tags
         fields = '__all__'

class NewsTagsSerializer(serializers.ModelSerializer):
    tag = TagsSerializer()
    news = NewsSerializer()
    class Meta:
         model = NewsTags
         fields = ('tag', 'news', 'newstag_id')



