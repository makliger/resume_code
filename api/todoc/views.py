from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import TokenAuthentication
from ooc_mobile_api.models import Tags, NewsTags,News, NewsFavorite, Breakfast, TopsPmTargets,  TopsPmTargetGroups, TopsPmEventStats, TopsPmTargetSummaryStats, TopsPmProcessStats, LogTable, VacationTable, MobileNotification, Deadline, VacationDateSchedule, BroadcastTemplate
from ooc_mobile_api.serializers import NewsTagsSerializer, TagsSerializer, LogSerializer, TargetSerializer,NewsFavoriteSerializer, NewsSerializer, OrderSerializer, EventSerializer, TargetTilesSerializer, TargetGraphsSerializer, MobileNotificationListSerializer, MobileNotificationCreateSerializer, MobileNotificationAppSerializer, GroupSerializer, DeadlineSerializer, BroadcastTemplateSerializer
import json, datetime, csv
from datetime import timedelta
from django.utils.timezone import now
from django.db import connections
import os, re
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.utils import timezone
import pytz
from ooc_mobile_api import ooc_pagination
from django.db.models import Q
from ooc_mobile_api.logs import tail_logs
from ooc_mobile_api.forms import NotificationForm
from django.views.generic.edit import FormView, CreateView, UpdateView
# Create your views here.
from rest_framework import generics, filters, pagination
from rest_framework import mixins
from fcm_django.models import FCMDevice
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
#def dictfetchall(cursor):
#    "Return all rows from a cursor as a dict"
#    columns = [col[0] for col in cursor.description]
#    return [
#        dict(zip(columns, row))
#        for row in cursor.fetchall()
#    ]
@csrf_exempt
@api_view(['POST'])
##@authentication_classes((AllowAny,))
@permission_classes((AllowAny,))
def login(request, format='json'):

    """
    Returns a user's current token and a few other attributes if called with valid credentials. This view does not require a token and is therefore accessible by anyone.
    
    Parameters
    ----------
    request
        HTTP request POST object with username, password and optional token
    """
   
    uname = request.POST.get('username')
    pword = request.POST.get('password')
    xToken = request.auth
    if xToken is None:
        if uname is None or pword is None:
            return Response({'error': 'one or more fields missing'}, status = status.HTTP_400_BAD_REQUEST)
    user = authenticate(username=uname, password=pword)
    if not user:
        return Response({'error': 'bad credentials'}, status = status.HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    full_name = user.get_full_name()
    idd = user.id
    user = User.objects.get(pk=idd)
    user.last_login=timezone.now()
    user.save()

    vt = VacationTable.objects.filter(user_id=user)
    if not vt:
        vt = VacationTable(user_id=user, status=False)
        vt.save()
    is_ops = False
    is_b = False
    for g in user.groups.all():
        if g.name == 'Operations':
            is_ops = True
        if g.name == 'Broadcast':
            is_b = True
    return Response({'token':token.key, 'full_name': full_name, 'employee_id' : idd, 'is_ops':is_ops, 'can_broadcast' : is_b}, status = status.HTTP_200_OK)


@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
@api_view(['POST'])
def register_device(request, pk, format='json'): ### lets us add devices to firebase model dynamically as long as they present a valid token

    """
    Returns success if valid credentials are given, after registering the user's device specific registration_id for firebase.
    
    Parameters
    ----------
    pk
        identifies the calling user.
    request
        HTTP request POST object with firebase cloud messaging key in body.
    """
    if request.method != 'POST':
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    user = User.objects.get(pk=pk)   ### todo use get rather than filter when single object is expected
    if not user:
        return Response(status=status.HTTP_404_NOT_FOUND)
    try:
        device = FCMDevice.objects.get(user=user)
    except FCMDevice.DoesNotExist:
        device = None
    key = request.POST.get("fcm_key", None)
    if not device:
        if key:
            device = FCMDevice(user=user,name=user.get_full_name(), active=True, date_created=timezone.now(), registration_id=key)
            device.save()
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
    else:
        if key:
            device.registration_id=key
            device.save()
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
    return Response(status=status.HTTP_200_OK)


@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
@api_view(['GET'])
def test_auth(request, format='json'):
    user = request.user
    return Response({"allowed" : request.user.is_authenticated})



@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
@api_view(['GET'])  ## handles list and folder view data
def plist(request, format='json'):
    """
    Returns success with latest proccess list stats if valid credentials are given and data is accessible.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    if request.method != 'GET':
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    try:
        p = TopsPmTargetSummaryStats.objects.all()
    except TopsPmTargetsSummaryStats.DoesNotExist:
        return Response(status = status.HTTP_404_NOT_FOUND)
    serializer = TargetSerializer(p, many = True)
    return(Response(serializer.data))

@authentication_classes((TokenAuthentication,))
@api_view(['POST'])
def process_restart(request, pk, format='json'):

    """
    Returns success and status if process is restarted. (Not implemented).
    
    Parameters
    ----------
    request
        HTTP request POST object.
    """
    if request.method != 'POST':
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    return(Response(status=status.HTTP_200_OK))


@authentication_classes((TokenAuthentication,))
@api_view(['GET'])  ## handles process details individually, per request
def pdetail(request, id, format='json'):
    """
    Returns success with latest proccess stats if valid credentials are given and data is accessible.
    
    Parameters
    ----------
    id
        identifies the current process.
    request
        HTTP request GET object.
    """
    if not (request.method == 'GET'):
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    if(request.method != 'POST'):
        try:
            p = TopsPmProcessStats.objects.filter(target=id)
        except TopsPmProcessStats.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)
        latest= reversed(p)
        final = list(p)
        res = final[0:49]
        res = reversed(res) #TODO does this make sense
        result = TargetGraphsSerializer(res, many = True)
        try:
            single_valued_data = TopsPmTargetSummaryStats.objects.get(target__target_id=id)
        except TopsPmTargetSummaryStats.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)
        sv_res = TargetTilesSerializer(single_valued_data, many=False)
        dict_json = {}
        dict_json['graphs'] = result.data
        dict_json['tiles'] = sv_res.data
        return Response({"graphs": result.data, "tiles":sv_res.data})

@authentication_classes((TokenAuthentication,))
@api_view(['GET'])
def news(request, id,  format='json'):
    """
    Returns a user's news and favorites.
    
    Parameters
    ----------
    id
        identifies the current user.
    request
        HTTP request GET object.
    """
    if(request.method != 'GET'):
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    range = 10
    e_id = id
    try:  ##### get user object
        us = User.objects.filter(id=e_id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if not us:
        return Response(status=status.HTTP_404_NOT_FOUND) ## maybe change this error code
    try:
        fav = NewsFavorite.objects.filter(user_id=us[0],is_active=True)
    except NewsFavorite.DoesNotExist:
        fav = ((),)

    fav_id_list = []
    for item in fav:
        if item.news_id_id not in fav_id_list:
            fav_id_list.append(item.news_id_id)
    f = News.objects.filter(news_id__in =fav_id_list)
    p = News.objects.filter(news_date__range=[timezone.now()-timedelta(days=range), timezone.now()])
    fin = (f | p).distinct()
    res = fin.order_by('-news_date')
    serializer = NewsSerializer(res, many = True)
    fserializer = NewsFavoriteSerializer(f, many = True)
    return(Response({'all': serializer.data, 'favorites' : fserializer.data}))

@authentication_classes((TokenAuthentication,))
@api_view(['POST'])
def save_news(request, pk, format='json'):
    """
    Returns success if valid credentials are given, and changes the value of the news favorite accordingly.
    
    Parameters
    ----------
    pk
        identifies the current process.
    request
        HTTP request GET object.
    """
    if request.method != 'POST':
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    e_id = pk
    article_id = request.POST.get('id', -1)
    if article_id == -1:
        return Response(status=status.HTTP_400_BAD_REQUEST) ## todo change this error code

    try: ##### get user object
        us = User.objects.filter(id=e_id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    try: ##### get news object
        news = News.objects.filter(news_id=article_id)
    except News.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    try:
        obj = NewsFavorite.objects.filter(user_id=us[0], news_id=news[0])
        if not obj:
            obj = NewsFavorite(news_id=news[0], user_id=us[0], is_active=True, last_modified=timezone.now())
            obj.save()
            return Response(status=status.HTTP_200_OK)
        boo = obj[0].is_active
        boo = not boo
        obj[0].is_active = boo
        obj[0].last_modified = timezone.now()
        obj[0].save(update_fields=['is_active', 'last_modified'])
    except NewsFavorite.DoesNotExist:
        obj = NewsFavorite(news_id=news[0], user_id=us[0], is_active=True, last_modified=timezone.now())
        obj.save()
        return Response(status=status.HTTP_200_OK)

    return Response(status = status.HTTP_200_OK)

##### logs and events
@authentication_classes((TokenAuthentication,))
@api_view(['GET'])
def logs(request,id, format='json'):
    """
    Returns logs of the current process. A bytes parameter can be set dynamically in the future. Currently, we use 8192 which results in about 56 logs on average.
    
    Parameters
    ----------
    id
        identifies the current process.
    request
        HTTP request GET object.
    """


    logz = tail_logs(id, 8192 ) ### bytes to read, 1024 yields around 7 log lines in superficial tests
    return(Response(reversed(logz)))



@authentication_classes((TokenAuthentication,))
@permission_classes((AllowAny,))
@api_view(['GET'])
def orders(request,pk, format='json'):
    """
    Returns a user's orders and their current order for this day, as well as their vactation status and schedule.
    
    Parameters
    ----------
    pk
        identifies the current user.
    request
        HTTP request GET object.
    """

    if(request.method != 'GET'):
         return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
   # try: ## todo change userid=1 to userid = args
   #      p = Breakfast.objects.raw("SELECT order_id, user_id, username, order_details,order_datetime FROM (SELECT ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY order_datetime) as most_recent_order_num, * FROM (SELECT ROW_NUMBER() OVER (PARTITION BY user_id, CONVERT(date, order_datetime) ORDER BY order_datetime DESC) as active_order_num, b.*, u.username FROM mobile_breakfast b LEFT JOIN auth_user u ON b.user_id = u.id WHERE u.is_active = 1 AND b.user_id = %s) active_orders WHERE active_order_num = 1) most_recent_orders WHERE most_recent_order_num <= 3 ORDER BY order_datetime DESC ;", [pk])
   # except Breakfast.DoesNotExist:
   #      return Response(status = status.HTTP_404_NOT_FOUND)
   # result = OrderSerializer(p, many=True)

####### code to extract last 3 latest orders from each day - doesnt exactly do what we need yet, should really be orders closest to 8am or whatever time each day
    rangeStart=0
    rangeEnd=1
    orders = []
    bodies = []
    lastOrder = "No orders posted from this account yet"
    n = len(orders)
    before_seven = Q(ordered_at__hour=0)
    for hour in range(1, 7):
        before_seven = before_seven | Q(ordered_at__hour=hour)
    while( n < 3 and rangeEnd < 90):
        order = Breakfast.objects.filter(order_date__range=[(timezone.now().date()-timedelta(days=rangeEnd)), (timezone.now().date()-timedelta(days=rangeStart))], user_id_id =pk).filter(before_seven)
        if not order:
            rangeEnd+=1
            rangeStart+=1
        elif(order.latest('ordered_at').pk in orders):
            rangeEnd+=1
            rangeStart+=1
        elif(order.latest('ordered_at').order in bodies):
            rangeEnd+=1
            rangeStart+=1
        elif('No breakfast today' in order.latest('ordered_at').order) :
            rangeEnd+=1
            rangeStart+=1
        else:
            if n == 0:
                lastOrder = order.latest('ordered_at').order
            orders.append(order.latest('ordered_at').pk)
            bodies.append(order.latest('ordered_at').order)
            n+=1
            rangeEnd+=1
            rangeStart+=1

    try:
        p = VacationTable.objects.filter(user_id_id=pk)
    except VacationTable.DoesNotExist:
        p = None
    qs = Breakfast.objects.filter(order_id__in= orders).order_by('-ordered_at')
    if not p:
        b=False
    else:
        b = p[0].status
    result = OrderSerializer(qs, many=True)

    todayOrder = Breakfast.objects.filter(user_id_id=pk).filter(order_date=timezone.now().date()).filter(before_seven).last()
    today = todayOrder
    if today:
        today = today.order
    else:
        today = "No order submitted. Using latest order: \n" + lastOrder

    v_sched = None
    if b:
        try:
            v_sched = VacationDateSchedule.objects.using('logging').get(user_id=pk)
        except VacationDateSchedule.DoesNotExist:
            v_sched = None

    vdate = "indefinitely"
    if v_sched:
        vdate = str(v_sched.month) + "/" + str(v_sched.day) + "/" + str(v_sched.year)

    return(Response({'orders': result.data, 'vacation':b, 'today': today, "schedule": vdate}))

@authentication_classes((TokenAuthentication,))
@api_view(['POST'])
def submit_order(request,pk, format='json'):
    """
    Returns success if order is saved to db. Submits order for next day via a time mapping if order is late.
    
    Parameters
    ----------
    id
        identifies the current User.
    request
        HTTP request POST object with order as its sole parameter.
    """
    if(request.method != 'POST'):
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    norder = request.POST.get('order', " ")
    if(norder == " "):
        return Response(status = status.HTTP_400_BAD_REQUEST)
    ntime = timezone.now()
    norder= norder.strip() 
    temp = ntime
    if ntime.hour > 6 and ntime.minute > 30: ### send to the next days order
        ntime = ntime - timedelta(hours = ntime.hour, minutes= ntime.minute, seconds=ntime.second)
        ntime = ntime + timedelta(minutes = 30, hours = 3, days = 1)
        seconds = timedelta(hours =temp.hour-6, minutes= temp.minute - 30, seconds= temp.second, microseconds = temp.microsecond).total_seconds()
        ###### map [0, 86400] -> [0, 1800]  ### this allows us to have a natural ordering on orders submitted for the next day
        ###### x / 86400 -> * 1800
        ###### here we map any order from 630 am on to a time in between 330-4 am the next day, by transforming seconds since 8am -12 am into the window of seconds between 330-430 
        ###### we use a 24 to 1 map, although it really should be 17.5 -> 1 hour since we cant enter here during the first 6.5 hours of the day. 
        fraction = seconds / 86400
        new_time = fraction * 1800
        ntime = ntime + timedelta(seconds = new_time)
    ndate =  ntime.date()
    new = Breakfast(user_id_id=pk, order=norder, ordered_at=ntime,order_date=ndate)
    new.save()
    ### TODO create signal to add confirmation message to history (with is_pub = true, so we wont get a push notification)
    return(Response(status = status.HTTP_200_OK))
##### use body in post


@authentication_classes((AllowAny,))
@api_view(['GET'])
def events(request,id,  format = 'json'):
    """
    Returns success with latest event stats of process if valid credentials are given and data is accessible. Not Implemented.
    
    Parameters
    ----------
    id
        identifies the current process.
    request
        HTTP request GET object.
    """
    if(request.method != 'GET'):
        return Response(status = status.HTTP_405_METHOD_NOT_ALLOWED)
    try:
        p = EventDataTable.objects.filter(target_id=id)
    except EventDataTable.DoesNotExist:
        return Response(status = status.HTTP_404_NOT_FOUND)
    result = EventSerializer(p, many=True)
    return (Response(result.data))

@authentication_classes((TokenAuthentication,))
@api_view(['POST'])
def submit_vacation_status(request, pk, format='json'):
    """
    Changes or creates a vacation status upon success. Returns status and schedule.
    
    Parameters
    ----------
    pk
        identifies the current User.
    request
        HTTP request POST object with status and schedule parameters..
    """
    if(request.method != 'POST'):
        return Response(status=status.HTTP_404_METHOD_NOT_ALLOWED)
    vstatus = request.POST.get('status', " ")
    vdate_picked = request.POST.get("vacation_date_picked", " ")
    vdate = None
    date = None
    if vdate_picked == 'true':
        vdate = request.POST.get("vacation_date", None)
    if vdate:
        date = datetime.datetime.strptime(vdate, '%m/%d/%Y').date()
        try:
            existing = VacationDateSchedule.objects.using('logging').get(user_id=pk)
        except VacationDateSchedule.DoesNotExist:
            existing = None
        if not existing:
            obj = VacationDateSchedule(user_id=pk, year=date.year, month=date.month, day=date.day, is_active=True)
            obj.save(using='logging')
        else:
            existing.year = date.year
            existing.month = date.month
            existing.day = date.day
            existing.is_active = 1
            existing.save(using='logging')
    if vstatus == " ":
        return(Response({"date": vdate, "picked" : vdate_picked, "stat": vstatus }, status=status.HTTP_400_BAD_REQUEST))
    
    vacation_sched_obj = None
    if vstatus == 'true':
        new = True
    else:
        new = False
        try:
            vacation_sched_obj = VacationDateSchedule.objects.using('logging').get(user_id=pk)
        except VacationDateSchedule.DoesNotExist:
            vacation_sched_obj = None
        if vacation_sched_obj:
            vacation_sched_obj.is_active = 0 ## sqlite so no booleans
            vacation_sched_obj.save(using='logging')

    obj, created= VacationTable.objects.update_or_create(user_id_id=pk, defaults={'status': new})
    return(Response({"truth" : vstatus, "obj" : str(vacation_sched_obj)}, status=status.HTTP_200_OK))




 #   try:
 #       obj = VacationTable.objects.get(user_id_id=pk)
 #   except VacationTable.DoesNotExist:
 #       obj = VacationTable(user_id=User.objects.get(pk=pk), status=new)
 #       obj.save()
 #
 #   res = obj.update(status=new)
 #   if res == 1:
 #       return(Response(status=status.HTTP_200_OK))
 #   return(Response(status=HTTP_400_BAD_REQUEST))



@authentication_classes((TokenAuthentication,))
@api_view(['GET'])
def messages(request, pk, format='json'):
    """
    Returns a user's latest messages and their groups upon success.
    
    Parameters
    ----------
    pk
        identifies the current User.
    request
        HTTP request GET object.
    """
    if(request.method != 'GET'):
        return Response(status=HTTP_404_METHOD_NOT_ALLOWED)
    debug = False
    is_pub = True
    range = 10
    #### debugging purposes
    if debug:
        range = 100
        is_pub = False
    msgs_us = MobileNotification.objects.filter(target_user_id=pk)
    groups =  []
    try:
        usr = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return Response(status=HTTP_403_FORBIDDEN)
    for g in usr.groups.all():
        groups.append(g.pk)
    msgs_gr = MobileNotification.objects.filter(target_user_group_id__in=groups)
    msgs_all = MobileNotification.objects.filter(target_user_group_id=None).filter(target_user_id=None) # this is all
    msgs = (msgs_us | msgs_gr | msgs_all).distinct()
    msgs = msgs.filter(target_publish_start__range=[(timezone.now()-timedelta(days=range)), timezone.now()])
    msgs = msgs.order_by('-target_publish_start')
    msgs = msgs.filter(is_published=is_pub) ## in case tghe publishing system fails, lets make it consistent

    #msgs = services.get_all_messages(usr, range)
    res = MobileNotificationAppSerializer(msgs, many=True)
    groups =  Group.objects.all()
    g_data = {}
    for g in groups:
        if g.name != 'Broadcast':
            g_data[g.pk] = g.name
        if g.name == 'All':
            g_data[g.pk] = 'Team'
    return Response({'messages': res.data, 'groups': g_data})

@api_view(['GET'])
@authentication_classes(())
@permission_classes((AllowAny,))
def todays_orders(request, format='json'):
    """
    Not currently used. Old version of mobile_todays_orders. Returns a csv file.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    res = []
    users = []
    orders = []
    maybeOrders = []
    vt = VacationTable.objects.all()
    before_seven = Q(ordered_at__hour=0)
    for i in range(1,7):
        before_seven= before_seven | Q(ordered_at__hour=i)

    for person in vt:
        if person.status is False:
            todayOrder = Breakfast.objects.filter(user_id=person.user_id).filter(order_date=timezone.now().date()).filter(before_seven).last()
            if todayOrder:
                orders.append({person.user_id.get_full_name() : todayOrder.order})
            else:
                lastOrder = Breakfast.objects.filter(user_id=person.user_id).filter(before_seven).last()
                if lastOrder:
                    maybeOrders.append({person.user_id.get_full_name() : lastOrder.order})
                else:
                    maybeOrders.append({person.user_id.get_full_name() : "no orders submitted from this account, verify"})
        else:
            res.append({person.user_id.get_full_name() : "no breakfast (vacation)"})


    orderList = []
    tOrders = orders.copy()
    tmaybeOrders =maybeOrders.copy()
    tres = res.copy()
    for i in tOrders:
        key, value = i.popitem()
        orderList.append({key : value})
    for j in tmaybeOrders:
        key, value = j.popitem()
        orderList.append({key: value})
    for k in tres:
        key, value = k.popitem()
        orderList.append({key :  value})

    first = len(tOrders)-1
    second = first+1
    third = first + len(tmaybeOrders)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="output.csv"'
    writer = csv.writer(response)
    for obj in orderList:
        key, value = obj.popitem()
        writer.writerow([key, value])

    return response

@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def mobile_todays_orders(request, format='json'):
    """
    Returns success with today's orders and vacation statuses.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    if request.method != 'GET':
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    res = []
    users = []
    orders = []
    maybeOrders = []
    vt = VacationTable.objects.all()
    before_seven = Q(ordered_at__hour=0)
    for i in range(1,7):
        before_seven= before_seven | Q(ordered_at__hour=i)
    results = {}
    for user in User.objects.all():
        results[user.get_full_name()] = {'vacation' : False, 'order' : "None", "today" : False}

    today = timezone.now().date()
    rangeStart= today - timedelta(days=120)


    for person in vt:
        if person.status is False:
            todayOrder = Breakfast.objects.filter(user_id=person.user_id).filter(order_date=timezone.now().date()).filter(before_seven).last()
            if todayOrder:
                orders.append({person.user_id.get_full_name() : todayOrder.order})
                results[person.user_id.get_full_name()]['order'] = todayOrder.order
                results[person.user_id.get_full_name()]['today'] = True
            else:
                lastOrder = Breakfast.objects.filter(user_id=person.user_id).filter(ordered_at__range=[rangeStart, today]).filter(before_seven).last()
                if lastOrder:
                    maybeOrders.append({person.user_id.get_full_name() : lastOrder.order})
                    results[person.user_id.get_full_name()]['order'] = lastOrder.order
                else:
                    maybeOrders.append({person.user_id.get_full_name() : "no orders submitted from this account, verify"})
                    results[person.user_id.get_full_name()]['order'] =  "no orders submitted from this account yet"

        else:
            results[person.user_id.get_full_name()]['order'] = "no breakfast (vacation)"
            results[person.user_id.get_full_name()]['vacation'] = True


    return Response(results, status=status.HTTP_200_OK)
    #return Response({'ordered_today' : orders, 'pending' : maybeOrders, 'vacation': res}, status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def remove_tag(request, pk, format='json'):
    """
    Returns success and changes tag last modified and active status.
    
    Parameters
    ----------
    pk
        identifies the user.
    request
        HTTP request POST object with tag_id
    """
    if request.method != 'POST':
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    tag_id = request.POST.get('tag', " ")
    user = User.objects.get(pk=pk)
    tag = Tags.objects.get(pk=tag_id)
    if not tag or not user:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    tag.is_active=False
    tag.last_modified=timezone.now()
    tag.save()
    return Response(status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def add_user_tag(request, pk, format='json'):
    """
    Returns success if tag is created or changed with the tag's id.
    
    Parameters
    ----------
    pk
        identifies the user
    request
        HTTP request POST object with tag name and notes.
    """
    if request.method != 'POST':
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    user = User.objects.get(pk=pk)
    tag_name = request.POST.get('tag_name', " ")
    tag_notes = request.POST.get('tag_notes', " ")
    if not user:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    try:
        tag = Tags.objects.get(tag_name=tag_name, user_id=user)
    except Tags.DoesNotExist:
        tag=None

    if not tag:
        tag = Tags(tag_name=tag_name, tag_notes=tag_notes, user_id=user, last_modified=timezone.now(), is_active=True, tag_type='mobile')
        tag.save()
    else:
        tag.is_active=True
        tag.last_modified=timezone.now()
        tag.tag_notes = tag_notes
        tag.save()
#    ut = UserTags.objects.get(user_id=user, tag_id=tag)
 #   if not ut:
  #      ut = UserTags(user_id=user, tag_id=tag, notes=tag_notes, last_modified=timezone.now(). is_active=True)
   #     ut.save()
 #   else:
 #       ut.is_active=True
 #       ut.last_modified=timezone.now()
 #       ut.save()
    return Response({"id": tag.tag_id}, status=status.HTTP_200_OK)

#    return Response({"orders" : orderList}) # "overview" : {"todays orders" : orderList[0:first], "orders that have not been submitted today (using last order)" : orderList[second:third], "employees supposedly on vacation mode" : orderList[third+1:]}})
@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def remove_user_tag(request, pk, format='json'):
    """
    Returns success if tag is accessible. Updates tag's is_active property.
    
    Parameters
    ----------
    pk
        identifies the user
    request
        HTTP request POST object with tag name.
    """
    if request.method != 'POST':
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
    user = User.objects.get(pk=pk)
    if not user:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    tag_name = request.POST.get('tag', " ")
    tag = Tags.objects.get(tag_name=tag_name, user_id=user)
    if not tag:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    tag.is_active=False
    tag.last_modified=timezone.now()
    return Response(status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def broadcast(request, pk, format='json'):
    """
    Returns success and recipient id's. Saves notification objects on per user basis. TODO : capture the group information here and in other notification functions as to not lose that descriptive data.
    
    Parameters
    ----------
    pk
        identifies the user
    request
        HTTP request POST object with recipients, body and title.
    """
    if request.method != 'POST':
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)
    user = User.objects.get(pk=pk)
    if not user:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    #title = request.POST.get('title', None)
    #body = request.POST.get('body', None)
    #groups = request.POST.get('groups', None)
    #ids = request.POST.get('users', None)
    data = json.loads(request.body.decode('utf-8'))
    users = data['users']
    groups = data['groups']
    body = data['body']
    title = data['title']
    toSend = []
    group = None
    if groups:
        group = Group.objects.filter(pk__in = groups)
    if group:
        for g in group:
            userInGroup = g.user_set.all()
            for user in userInGroup:
                if user.pk not in toSend:
                    toSend.append(user.pk)

    if users:
        user_qs = User.objects.filter(pk__in = users)
        for user in user_qs:
            if user.pk not in toSend:
                toSend.append(user.pk)

    for id in toSend:
        user = User.objects.get(pk=id)
        mn = MobileNotification(target_user_id=user, msg_header=title, msg_body=body, is_published=False, target_publish_start=timezone.now())
        mn.save()

    return Response({'data': toSend},status=status.HTTP_200_OK)


@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def broadcast_template(request, format='json'):
    """
    Returns template for preset messages data and mobile deadlines.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    presets = BroadcastTemplate.objects.all()
    rest = BroadcastTemplateSerializer(presets, many=True)
    debugone = BroadcastTemplate(title="Reminder", message="Investors are coming today, please dress accordingly")
    debugtwo = BroadcastTemplate(title="Hello", message="this is a test")
    debug_presets = BroadcastTemplateSerializer([debugone, debugtwo], many=True)
    result = None
    if not presets:
        result = debug_presets
    else:
        result = rest


    objs = Deadline.objects.using('logging').all()
    res = DeadlineSerializer(objs, many=True)
    return Response({"deadline": res.data, "template": result.data}, status=status.HTTP_200_OK)



@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def get_user_groups(request, format='json'):
    """
    Returns users and groups.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    users = {}
    groups = {}
    for user in User.objects.exclude(username='test').exclude(username="tester"):
       users[user.pk] = user.get_full_name()
    for g in Group.objects.all():
       groups[g.pk] = g.name
    return Response({"users":users, "groups":groups}, status=status.HTTP_200_OK)

@api_view(['GET'])
@authentication_classes((TokenAuthentication,))
@permission_classes((AllowAny,))
def deadline(request, format='json'):

    """
    Not in use, see broadcast_template function.
    
    Parameters
    ----------
    request
        HTTP request GET object.
    """
    presets = BroadcastTemplate.objects.all()
    res = BroadcastTemplateSerializer(presets, many=True)
    debugone = BroadcastTemplate(title="Reminder", message="Investors are coming today, please dress accordingly")
    debugtwo = BroadcastTemplate(title="Hello", message="this is a test")
    debug_presets = BroadcastTemplateSerializer([debugone, debugtwo], many=True)
    result = None
    if not presets:
        result = debug_presets
    else:
        result = res


    objs = Deadline.objects.using('logging').all()
    res = DeadlineSerializer(objs, many=True)
    return Response(res.data, status=status.HTTP_200_OK)
#class UserTagsList(generics.ListCreateAPIView):
#    filter_backends=[DjangoFilterBackend]
#    authentication_classes=[]
#    permission_classes=[AllowAny]

#    def get_queryset(self):
#        user = self.request.user
#        tags=  UserTags.objects.filter(user_id=user)
#        res = []
#        for ut in tags:
#            res.append(ut.tag_id.pk)
#        return Tags.objects.filter(pk_in__range=res)

class NotificationList(generics.ListCreateAPIView):
    authentication_classes= [TokenAuthentication]
#    permission_classes=[AllowAny]
    queryset = MobileNotification.objects.all().order_by('-target_publish_start')
    serializer_class = MobileNotificationListSerializer
    pagination_class = ooc_pagination.ModeratePagination
    filter_backends=[DjangoFilterBackend]
    filterset_fields = ['target_user_group_id', 'target_user_id']

class NotificationDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes= [TokenAuthentication]
    queryset = MobileNotification.objects.all()
    serializer_class = MobileNotificationCreateSerializer




### Class Based Views ###
#@authentication_classes((TokenAuthentication,))
#class NotificationCreateView(CreateView):
#    model = MobileNotification
#    fields = ['msg_header', 'msg_body', 'msg_footer', 'target_user_id', 'target_user_group_id', 'target_publish_start']
#    template_name = "MobileNotification_form.html"


class DeadlineListCreate(generics.ListCreateAPIView):

    authentication_classes=[TokenAuthentication]
    permission_classes = [AllowAny]
    serializer_class = DeadlineSerializer
    queryset = Deadline.objects.using('logging').all()
    def create(self, request, *args, **kwargs):
        pk = request.POST.get('id', None)
        nname= request.POST.get('name', None)
        nhr = request.POST.get('hour', None)
        nmin = request.POST.get('minute', None)
        if pk and nhr and nmin:
            d = Deadline(id=pk, name=nname, hour=nhr, minute=nmin)
            d.save(using='logging')
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Respnse(status=status.HTTP_404_BAD_REQUEST)
class DeadlineUpdate(generics.RetrieveUpdateAPIView):
    queryset = Deadline.objects.using('logging').all()
    permission_classes=[AllowAny]
    authentication_classes=[TokenAuthentication]
    serializer_class = DeadlineSerializer
    def update(self, request, *args, **kwargs):
        pk = request.data.get('id', None)
        nname= request.data.get('name', None)
        nhr = request.data.get('hour', None)
        nmin = request.data.get('minute', None)
        if pk and nhr and nmin:
            d = Deadline.objects.using('logging').get(pk=pk)
            d.name = nname
            d.hour = nhr
            d.minute = nmin
            d.save(using='logging')
            return Response(status=status.HTTP_200_OK)
        return Response(status=HTTP_404_BAD_REQUEST)


### tags
class UserTagsUpdate(generics.UpdateAPIView):
    authentication_classes=[]
    permission_classes=[AllowAny]
    serializer_class=TagsSerializer
    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return Tags.objects.all()
        return Tags.objects.filter(user_id=user)


class UserTagsListCreate(generics.ListCreateAPIView):
    authentication_classes=[TokenAuthentication]
    permission_classes=[IsAuthenticated]
    serializer_class = TagsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['tag_name', 'tag_notes']
    def get_queryset(self):
        user = self.request.user
        if user.is_anonymous:
            return Tags.objects.all()
        return Tags.objects.filter(user_id=user).filter(is_active=True)

class UserTagsAdminView(generics.RetrieveUpdateDestroyAPIView):
    queryset=Tags.objects.all()
    authentication_classes=[]
    permission_classes=[AllowAny]
    serializer_class = TagsSerializer

class TagsListCreate(generics.ListCreateAPIView):
    authentication_classes=[TokenAuthentication]
    queryset = Tags.objects.all()
    serializer_class = TagsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['tag_name', 'tag_notes']

class NewsTagsListCreate(generics.ListCreateAPIView):
    queryset = NewsTags.objects.all()
    authentication_classes=[TokenAuthentication]
    serializer_class = NewsTagsSerializer


class TagsListView(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    queryset = Tags.objects.all()
    serializer_class = TagsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['tag_name', 'tag_notes']

class NewsTagsListView(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    queryset = NewsTags.objects.all()
    serializer_class = NewsTagsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['news__news_title', 'tag__tag_name', 'news__news_author']

class NewsSearchByTagsListView(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    queryset = NewsTags.objects.all()
    serializer_class = NewsTagsSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['tag__tag_name', 'tag__tag_notes']

class NewsSearchListView(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    queryset = News.objects.all().order_by('-news_date')
    serializer_class = NewsSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['news_title', 'news_author', 'source', 'contents']







