#!/bin/bash
cd /webapps/ooc_server
. api/bin/activate

python /webapps/ooc_server/ooc_mobile/ooc_mobile_api/notifications.py >> /webapps/ooc_server/tmp/cron_logs.txt


######## crontab has problems running python, venv, other things; this seems to be a dirty fix but the show must go on
