import os, re, django, sys, pytz
import datetime
sys.path.append("/webapps/ooc_server/ooc_mobile")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooc_mobile.settings")
django.setup()
from django.conf import settings

from ooc_mobile_api.models import MobileNotification
from django.contrib.auth.models import User, Group
from django.utils import timezone
import pytz

group = Group.objects.get(pk=1)
user = User.objects.get(pk=3)
time = timezone.now()
title = "test"
subtitle = "test_footer"


notification = MobileNotification(target_user_group_id=group, msg_header=title, msg_body=subtitle, target_publish_start=time, is_published=False)
notification.save()

#notification = MobileNotification(target_user_id=user, msg_header=title, msg_body=subtitle, target_publish_start=time, is_published=False)
#notification.save()

#user= User.objects.get(pk=2)
#notification = MobileNotification(target_user_id=user, msg_header=title, msg_body=subtitle, target_publish_start=time, is_published=False)
#notification.save()
